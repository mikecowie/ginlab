#!/bin/bash
set -e
cat builds | while read line; do
  set -- $line
  mkdir -p test
  cat <<EOF> test/main.tf
  resource "null_resource" "null" {}
EOF
  cat <<EOF> test/run.sh
  #!/bin/bash
  set -e
  cd /test
  terraform init
  terraform plan -out plan
  terraform apply plan
  terraform destroy -auto-approve
EOF
  docker pull $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA
  docker run $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA terraform version
  docker run -v $(pwd)/test:/test --entrypoint /bin/bash $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA  /test/run.sh
  docker tag $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_REF_NAME
  docker tag $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA $CI_REGISTRY_IMAGE/terraform:$1-master
  docker push $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_REF_NAME
  docker push $CI_REGISTRY_IMAGE/terraform:$1-master
done
