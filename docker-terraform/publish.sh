#!/bin/bash
set -e
cat builds | while read line; do
  set -- $line
  docker pull $CI_REGISTRY_IMAGE/terraform:$1-master
  docker tag $CI_REGISTRY_IMAGE/terraform:$1-master $CI_REGISTRY_IMAGE/terraform:$1
  docker tag $CI_REGISTRY_IMAGE/terraform:$1-master $CI_REGISTRY_IMAGE/terraform:$1
  docker push $CI_REGISTRY_IMAGE/terraform:$1
done
