#!/bin/bash
set -e
cat builds | while read line; do
  set -- $line
  mkdir -p build
  echo "Variables read as $1 $2 $3 $4"
  cp Dockerfile.template build/Dockerfile.$1
  sed -i 's#__URL__#'$2'#' build/Dockerfile.$1
  sed -i 's#__FILENAME__#'$3'#' build/Dockerfile.$1
  sed -i 's#__SHA256SUM__#'$4'#' build/Dockerfile.$1
  docker pull $CI_REGISTRY_IMAGE/terraform:$1 || docker pull $CI_REGISTRY_IMAGE/terraform:$1-master || true
  docker build --cache-from $CI_REGISTRY_IMAGE/terraform:$1-master --tag $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA -f build/Dockerfile.$1 build/
  docker push $CI_REGISTRY_IMAGE/terraform:$1-$CI_COMMIT_SHA
done
