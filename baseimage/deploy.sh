#!/bin/bash
qm create 4002 --name centos-$(date -I) --bootdisk ide2 --virtio0 ssd-vm:8,cache=writeback,format=qcow2  --net0 virtio,bridge=vmbr1,firewall=1,tag=40 --cdrom iso:iso/CentOS-Kickstart-2019-05.iso --ostype l26 --memory 2048 --cores 2
qm start 4002
#For some reason network is incorrect until VM is reset
#sleep 30
#qm reset 4002
#wait until the following command returns stopped, then change the boot disk to the main hdd
finished=0
until  [ $finished -eq 1 ] ; do
        finished=$(qm list | grep 4002 | grep stopped | wc -l)
	echo "waiting for image build to complete"
        echo $finished
	qm list | grep 4002
	sleep 5
done
qm set 4002 --bootdisk virtio0  --template=1 --ide2 iso:cloudinit

