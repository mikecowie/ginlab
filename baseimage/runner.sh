#!/bin/bash
#configures gitlab-runner instances with details from a runners file

#runners file not to be commited to git, but a runners.example file will document the format required.

while read line; do
	echo "$line"
	GITLAB_CI_TOKEN=$(echo $line | xargs | cut -f 1 -d " ")
	DESCRIPTION=$(echo $line | xargs | cut -f 2 -d " ")
	/usr/local/bin/gitlab-runner  list 2>&1  grep $DESCRIPTION || /usr/local/bin/gitlab-runner register -n --url https://gitlab.com/ --registration-token $GITLAB_CI_TOKEN --executor docker --description "$DESCRIPTION" --docker-image "docker:stable" --docker-privileged
done < runners
