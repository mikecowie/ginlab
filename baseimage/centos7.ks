
text
install
lang en_NZ.UTF-8
keyboard us
timezone Pacific/Auckland
auth --useshadow --enablemd5
eula --agreed
ignoredisk --only-use=vda
shutdown

bootloader --location=mbr
zerombr
clearpart --all --initlabel
part /boot --fstype xfs --size=200
part pv.01 --size=1 --grow
volgroup vg01 pv.01
logvol swap --name=swap --vgname=vg01 --fstype="swap" --size=1024
logvol / --fstype xfs --name=root --vgname=vg01 --size=1 --grow

user --name centos --groups wheel


url --url=http://ucmirror.canterbury.ac.nz/linux/centos/7/os/x86_64/
repo --name=updates --baseurl=http://ucmirror.canterbury.ac.nz/linux/centos/7/updates/x86_64/


network --bootproto=static --ip=192.168.4.2 --netmask=255.255.255.0 --gateway=192.168.4.1 --nameserver=8.8.8.8

%packages --nobase --ignoremissing
@core
cloud-init
%end
