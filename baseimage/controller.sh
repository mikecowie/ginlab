#!/bin/bash
qm list | grep 4003 || qm clone 4002 4003
qm set 4003 --name=mc-controller
qm set 4003 --ciuser=centos
qm set 4003 --ipconfig0 ip=192.168.4.3/24,gw=192.168.4.1
qm set 4003 --nameserver 8.8.8.8
qm set 4003 --sshkeys /root/.ssh/id_rsa.pub
qm start 4003
#sleep 30
#qm stop 4003
#qm destroy 4003
ssh centos@192.168.4.3 sudo yum update -y
ssh centos@192.168.4.3 sudo yum install -y ansible rsync
rsync -av baseimage/ansible/ centos@192.168.4.3:/home/centos/ansible/
ssh centos@192.168.4.3 sudo ansible-playbook ansible/controller.yml
rsync baseimage/runner.sh centos@192.168.4.3:/home/centos/runner.sh
rsync baseimage/runners centos@192.168.4.3:/home/centos/runners
ssh centos@192.168.4.3 sudo bash runner.sh
