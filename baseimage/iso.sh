#!/bin/bash
mkdir -p assets
cd assets
if [ ! -f CentOS-7-x86_64-Minimal-1810.iso ] ; then
	curl -O http://ucmirror.canterbury.ac.nz/linux/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso
fi
mkdir -p isomount
mkdir -p isobuild
mount -t iso9660 -o loop CentOS-7-x86_64-Minimal-1810.iso $(pwd)/isomount
cd isomount
tar cf - . | (cd ../isobuild; tar xfp -)
cp ../../baseimage/isolinux.cfg ../isobuild/isolinux/
cp ../../baseimage/centos7.cfg ../isobuild/isolinux/ks.cfg
cd ../isobuild
#genisoimage -hard-disk-boot -b 'isolinux/isolinux.bin' -c 'isolinux/boot.cat' -v -J -r -V CentOS -o ../CentOS-Kickstart-$(date +%Y-%m).iso -boot-load-size 4 -boot-info-table  .
pwd
genisoimage -l -r -J -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -c isolinux/boot.cat -o ../CentOS-Kickstart-$(date +%Y-%m).iso -V CENTOS . 
cd ..
umount $(pwd)/isomount
rm -rf isobuild/

cp CentOS-Kickstart-$(date +%Y-%m).iso /mnt/fast_rust/store/iso/template/iso/
