.PHONY: baseimage controller

iso:
	bash baseimage/iso.sh
baseimage:
	bash baseimage/deploy.sh

baseimage-destroy:
	qm stop 4002 ; qm destroy 4002

controller:
	bash baseimage/controller.sh

	build-terraform

build:
	cd docker-terraform && bash build.sh
